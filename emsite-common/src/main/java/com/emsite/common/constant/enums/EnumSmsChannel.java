package com.emsite.common.constant.enums;

import lombok.Getter;
import lombok.Setter;
/**
* @Description: 短信通道枚举
* @author lix
* @date 2018/8/20 14:43
*/
public enum EnumSmsChannel {
    /**
     * 阿里大鱼短信通道
     */
    ALIYUN("ALIYUN_SMS", "阿里大鱼");
    /**
     * 通道名称
     */
    @Getter
    @Setter
    private String name;
    /**
     * 通道描述
     */
    @Getter
    @Setter
    private String description;

    EnumSmsChannel(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
