package com.emsite.common.constant;
/**
* @Description: 服务名称维护
* @author lix
* @date 2018/8/20 14:45
*/
public interface ServiceNameConstant {
    /**
     * 认证服务的SERVICEID（zuul 配置的对应）
     */
    String AUTH_SERVICE = "emsite-auth";

    /**
     * 系统信息模块
     */
    String UPMS_SERVICE = "emsite-upms";
}