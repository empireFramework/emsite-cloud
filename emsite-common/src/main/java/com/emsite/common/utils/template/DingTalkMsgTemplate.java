/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.emsite.common.utils.template;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
/**
* @Description: 钉钉消息模板
* @author lix
* @date 2018/8/20 14:46
*/
@Data
@ToString
public class DingTalkMsgTemplate implements Serializable {
    private String msgtype;
    private TextBean text;
    @Data
    public static class TextBean {
        /**
         * content : 服务: pig-upms-service 状态：UP
         */
        private String content;
    }
}
