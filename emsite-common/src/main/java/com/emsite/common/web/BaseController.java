package com.emsite.common.web;

import com.emsite.common.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* @Description: 基础控制器
* @author lix
* @date 2018/8/20 14:57
*/
public class BaseController {
    @Autowired
    private HttpServletRequest request;

    /**
     * 根据请求heard中的token获取用户角色
     * @return 角色名
     */
    public List<String> getRole() {
        return UserUtils.getRole(request);
    }

    /**
     * 根据请求heard中的token获取用户ID
     * @return 用户ID
     */
    public Integer getUserId() {
        return UserUtils.getUserId(request);
    }
}
