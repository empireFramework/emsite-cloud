/*
Navicat MySQL Data Transfer

Source Server         : trest
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : emsite

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2018-08-18 22:57:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '部门名称',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `del_flag` char(1) DEFAULT '0' COMMENT '是否删除  -1：已删除  0：正常',
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='部门管理';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('1', '济南研发中心', '1', '2018-01-22 19:00:23', '2018-08-06 19:39:13', '0', '0');
INSERT INTO `sys_dept` VALUES ('2', '济南运维中心', '1', '2018-01-22 19:00:38', '2018-08-06 19:39:29', '0', '0');
INSERT INTO `sys_dept` VALUES ('40', '云计算', '1', '2018-08-06 19:39:46', null, '0', '1');
INSERT INTO `sys_dept` VALUES ('41', '大数据', '2', '2018-08-06 19:40:40', null, '0', '1');
INSERT INTO `sys_dept` VALUES ('42', '运维一', '1', '2018-08-06 19:40:54', null, '0', '2');
INSERT INTO `sys_dept` VALUES ('43', '运维二', '2', '2018-08-06 19:41:10', null, '0', '2');

-- ----------------------------
-- Table structure for `sys_dept_relation`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept_relation`;
CREATE TABLE `sys_dept_relation` (
  `ancestor` int(11) NOT NULL COMMENT '祖先节点',
  `descendant` int(11) NOT NULL COMMENT '后代节点',
  PRIMARY KEY (`ancestor`,`descendant`) USING BTREE,
  KEY `idx1` (`ancestor`) USING BTREE,
  KEY `idx2` (`descendant`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_dept_relation
-- ----------------------------
INSERT INTO `sys_dept_relation` VALUES ('40', '40');
INSERT INTO `sys_dept_relation` VALUES ('41', '41');
INSERT INTO `sys_dept_relation` VALUES ('42', '42');
INSERT INTO `sys_dept_relation` VALUES ('43', '43');

-- ----------------------------
-- Table structure for `sys_dict`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` int(64) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `value` varchar(100) NOT NULL COMMENT '数据值',
  `label` varchar(100) NOT NULL COMMENT '标签名',
  `type` varchar(100) NOT NULL COMMENT '类型',
  `description` varchar(100) NOT NULL COMMENT '描述',
  `sort` decimal(10,0) NOT NULL COMMENT '排序（升序）',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1', 'true', 'isLogin', 'system', '是否登录', '1', '2018-08-14 09:19:49', '2018-08-14 09:19:49', '1', '0');

-- ----------------------------
-- Table structure for `sys_gateway_route`
-- ----------------------------
DROP TABLE IF EXISTS `sys_gateway_route`;
CREATE TABLE `sys_gateway_route` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'router Id',
  `path` varchar(255) NOT NULL COMMENT '路由路径',
  `serviceId` varchar(255) NOT NULL COMMENT '服务名称',
  `url` varchar(255) DEFAULT NULL COMMENT 'url代理',
  `strip_prefix` char(1) DEFAULT '1' COMMENT '转发去掉前缀',
  `retryable` char(1) DEFAULT '1' COMMENT '是否重试',
  `enabled` char(1) DEFAULT '1' COMMENT '是否启用',
  `sensitiveHeaders_list` varchar(255) DEFAULT NULL COMMENT '敏感请求头',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标识（0-正常,1-删除）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='动态路由配置表';

-- ----------------------------
-- Records of sys_gateway_route
-- ----------------------------
INSERT INTO `sys_gateway_route` VALUES ('2', '/upms/**', 'emsite-upms', '', '1', '1', '1', '', '2018-05-21 11:40:38', '2018-08-18 22:31:42', '0');
INSERT INTO `sys_gateway_route` VALUES ('3', '/uaa/**', 'emsite-auth', '', '1', '1', '1', '', '2018-05-21 11:41:08', '2018-08-18 22:31:33', '0');
INSERT INTO `sys_gateway_route` VALUES ('4', 'dasd', 'kcloud-yyy', 'asdasd', '0', '0', '0', 'asdas', '2018-08-08 16:49:44', '2018-08-08 16:51:13', '1');
INSERT INTO `sys_gateway_route` VALUES ('5', 'asd', 'asd', 'asd', '0', '0', '0', 'asd', '2018-08-08 16:52:54', '2018-08-08 16:52:59', '1');
INSERT INTO `sys_gateway_route` VALUES ('6', 'as', 'sa', 'as', '0', '0', '0', 'as', '2018-08-08 16:54:09', '2018-08-08 16:54:20', '1');
INSERT INTO `sys_gateway_route` VALUES ('7', 'qwe', 'qwe', 'qwe', '0', '0', '0', 'qwe', '2018-08-08 16:58:49', '2018-08-08 16:59:23', '1');
INSERT INTO `sys_gateway_route` VALUES ('8', 'qwe', 'qwe', 'qwe', '0', '0', '0', 'qw', '2018-08-08 17:01:03', '2018-08-08 17:04:56', '1');
INSERT INTO `sys_gateway_route` VALUES ('9', 'qweq', 'weqw', 'we', '0', '0', '0', 'qwe', '2018-08-08 17:05:22', '2018-08-08 17:05:25', '1');
INSERT INTO `sys_gateway_route` VALUES ('10', 'wqerqw', 'wer', 'werwerqwrqwerqwerqrewqrqwerwerwerwqwerwqerweqrweqrweqrweqrweqrwqeqerwqerweqreqwrqwerqwerwqerwqerwqerqwerwqerwqerwqerwqerqwerqwerqwerqwerwqe', '0', '0', '0', 'qwer', '2018-08-08 17:06:33', '2018-08-08 17:06:43', '1');
INSERT INTO `sys_gateway_route` VALUES ('11', 'qwe', 'qwe', 'qweqweqweqweqweqweqweqweqweqeqweqwweqweqweqweqweqwweqwewqeqwewqqeqweqwweqwweqweqweqwweqweqweqeqwewqeqwwwqweqweqwe', '0', '0', '0', 'eqweqww', '2018-08-08 17:07:59', '2018-08-08 17:22:47', '1');

-- ----------------------------
-- Table structure for `sys_log_0`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_0`;
CREATE TABLE `sys_log_0` (
  `id` bigint(64) NOT NULL COMMENT '编号',
  `type` char(1) DEFAULT '1' COMMENT '日志类型',
  `title` varchar(255) DEFAULT '' COMMENT '日志标题',
  `service_id` varchar(32) DEFAULT NULL COMMENT '服务ID',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remote_addr` varchar(255) DEFAULT NULL COMMENT '操作IP地址',
  `user_agent` varchar(1000) DEFAULT NULL COMMENT '用户代理',
  `request_uri` varchar(255) DEFAULT NULL COMMENT '请求URI',
  `method` varchar(10) DEFAULT NULL COMMENT '操作方式',
  `params` text COMMENT '操作提交的数据',
  `time` mediumtext COMMENT '执行时间',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标记',
  `exception` text COMMENT '异常信息',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_log_create_by` (`create_by`) USING BTREE,
  KEY `sys_log_request_uri` (`request_uri`) USING BTREE,
  KEY `sys_log_type` (`type`) USING BTREE,
  KEY `sys_log_create_date` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='日志表';

-- ----------------------------
-- Records of sys_log_0
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_log_1`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_1`;
CREATE TABLE `sys_log_1` (
  `id` bigint(64) NOT NULL COMMENT '编号',
  `type` char(1) DEFAULT '1' COMMENT '日志类型',
  `title` varchar(255) DEFAULT '' COMMENT '日志标题',
  `service_id` varchar(32) DEFAULT NULL COMMENT '服务ID',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remote_addr` varchar(255) DEFAULT NULL COMMENT '操作IP地址',
  `user_agent` varchar(1000) DEFAULT NULL COMMENT '用户代理',
  `request_uri` varchar(255) DEFAULT NULL COMMENT '请求URI',
  `method` varchar(10) DEFAULT NULL COMMENT '操作方式',
  `params` text COMMENT '操作提交的数据',
  `time` mediumtext COMMENT '执行时间',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标记',
  `exception` text COMMENT '异常信息',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_log_create_by` (`create_by`) USING BTREE,
  KEY `sys_log_request_uri` (`request_uri`) USING BTREE,
  KEY `sys_log_type` (`type`) USING BTREE,
  KEY `sys_log_create_date` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='日志表';

-- ----------------------------
-- Records of sys_log_1
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  `parent_id` int(11) DEFAULT NULL COMMENT '父菜单ID',
  `name` varchar(32) NOT NULL COMMENT '菜单名称',
  `type` char(1) DEFAULT NULL COMMENT '菜单类型 （0菜单 1按钮）',
  `permission` varchar(32) DEFAULT NULL COMMENT '菜单权限标识',
  `url` varchar(128) DEFAULT NULL COMMENT '请求链接',
  `method` varchar(32) DEFAULT NULL COMMENT '请求方法',
  `path` varchar(128) DEFAULT NULL COMMENT '前端URL',
  `icon` varchar(32) DEFAULT NULL COMMENT '图标',
  `component` varchar(64) DEFAULT NULL COMMENT 'VUE页面',
  `sort` int(11) DEFAULT '1' COMMENT '排序值',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `del_flag` char(1) DEFAULT '0' COMMENT '0--正常 1--删除',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('0', '-1', 'KCloud管理平台', '0', null, null, null, null, null, null, '1', '2018-08-13 00:59:53', '2018-08-13 01:03:55', '0');
INSERT INTO `sys_menu` VALUES ('1', '0', '系统管理', '0', null, null, null, '/upms', 'icon-xitongguanli', 'Layout', '1', '2017-11-07 20:56:00', '2018-08-13 01:03:57', '0');
INSERT INTO `sys_menu` VALUES ('2', '0', '系统监控', '0', null, null, null, '', 'icon-jiankong', null, '2', '2018-01-22 12:30:41', '2018-08-13 01:04:00', '0');
INSERT INTO `sys_menu` VALUES ('3', '1', '菜单管理', '0', null, '', null, 'menu', 'icon-caidanguanli', 'views/admin/menu/index', '1', '2017-11-08 09:57:27', '2018-08-13 01:06:00', '0');
INSERT INTO `sys_menu` VALUES ('4', '1', '部门管理', '0', null, null, null, 'dept', 'icon-bumenguanli', 'views/admin/dept/index', '2', '2018-01-20 13:17:19', '2018-08-13 01:06:00', '0');
INSERT INTO `sys_menu` VALUES ('5', '1', '用户管理', '0', null, '', null, 'user', 'icon-yonghuguanli', 'views/admin/user/index', '3', '2017-11-02 22:24:37', '2018-08-13 01:06:00', '0');
INSERT INTO `sys_menu` VALUES ('6', '1', '角色管理', '0', null, null, null, 'role', 'icon-jiaoseguanli', 'views/admin/role/index', '4', '2017-11-08 10:13:37', '2018-08-13 01:06:00', '0');
INSERT INTO `sys_menu` VALUES ('7', '1', '字典管理', '0', null, null, null, 'dict', 'icon-shujuzidian', 'views/admin/dict/index', '5', '2017-11-29 11:30:52', '2018-08-13 01:06:00', '0');
INSERT INTO `sys_menu` VALUES ('8', '1', '日志管理', '0', null, null, null, 'log', 'icon-rizhiguanli', 'views/admin/log/index', '6', '2017-11-20 14:06:22', '2018-08-13 01:06:00', '0');
INSERT INTO `sys_menu` VALUES ('9', '1', '路由管理', '0', null, null, null, 'route', 'icon-luyou', 'views/admin/route/index', '7', '2018-05-15 21:44:51', '2018-08-13 01:13:05', '0');
INSERT INTO `sys_menu` VALUES ('10', '1', '客户端管理', '0', '', '', '', 'client', 'icon-client', 'views/admin/client/index', '8', '2018-01-20 13:17:19', '2018-08-13 01:13:01', '0');
INSERT INTO `sys_menu` VALUES ('11', '3', '菜单查看', '1', null, '/upms/menu/**', 'GET', null, null, null, null, '2017-11-08 09:57:56', '2018-08-13 01:13:55', '0');
INSERT INTO `sys_menu` VALUES ('12', '3', '菜单新增', '1', 'sys_menu_add', '/upms/menu/*', 'POST', null, null, null, null, '2017-11-08 10:15:53', '2018-08-13 01:13:56', '0');
INSERT INTO `sys_menu` VALUES ('13', '3', '菜单修改', '1', 'sys_menu_edit', '/upms/menu/*', 'PUT', null, null, null, null, '2017-11-08 10:16:23', '2018-08-13 01:13:57', '0');
INSERT INTO `sys_menu` VALUES ('14', '3', '菜单删除', '1', 'sys_menu_del', '/upms/menu/*', 'DELETE', null, null, null, null, '2017-11-08 10:16:43', '2018-08-13 01:13:57', '0');
INSERT INTO `sys_menu` VALUES ('15', '4', '部门查看', '1', '', '/upms/dept/**', 'GET', null, null, '', null, '2018-01-20 13:17:19', '2018-08-13 01:13:59', '0');
INSERT INTO `sys_menu` VALUES ('16', '4', '部门新增', '1', 'sys_dept_add', '/upms/dept/**', 'POST', null, null, null, null, '2018-01-20 14:56:16', '2018-08-13 01:14:01', '0');
INSERT INTO `sys_menu` VALUES ('17', '4', '部门修改', '1', 'sys_dept_edit', '/upms/dept/**', 'PUT', null, null, null, null, '2018-01-20 14:56:59', '2018-08-13 01:14:01', '0');
INSERT INTO `sys_menu` VALUES ('18', '4', '部门删除', '1', 'sys_dept_del', '/upms/dept/**', 'DELETE', null, null, null, null, '2018-01-20 14:57:28', '2018-08-13 01:14:02', '0');
INSERT INTO `sys_menu` VALUES ('19', '5', '用户查看', '1', '', '/upms/user/**', 'GET', null, null, null, null, '2017-11-07 20:58:05', '2018-08-13 01:14:03', '0');
INSERT INTO `sys_menu` VALUES ('20', '5', '用户新增', '1', 'sys_user_add', '/upms/user/*', 'POST', null, null, null, null, '2017-11-08 09:52:09', '2018-08-13 01:14:04', '0');
INSERT INTO `sys_menu` VALUES ('21', '5', '用户修改', '1', 'sys_user_upd', '/upms/user/**', 'PUT', null, null, null, null, '2017-11-08 09:52:48', '2018-08-13 01:14:06', '0');
INSERT INTO `sys_menu` VALUES ('22', '5', '用户删除', '1', 'sys_user_del', '/upms/user/*', 'DELETE', null, null, null, null, '2017-11-08 09:54:01', '2018-08-13 01:14:07', '0');
INSERT INTO `sys_menu` VALUES ('23', '5', '密码重置', '1', 'sys_user_pwd', '/upms/user/**', 'PUT', '', '', '', null, '2018-08-01 14:09:18', '2018-08-13 01:14:08', '0');
INSERT INTO `sys_menu` VALUES ('24', '6', '角色查看', '1', null, '/upms/role/**', 'GET', null, null, null, null, '2017-11-08 10:14:01', '2018-08-13 01:15:31', '0');
INSERT INTO `sys_menu` VALUES ('25', '6', '角色新增', '1', 'sys_role_add', '/upms/role/*', 'POST', null, null, null, null, '2017-11-08 10:14:18', '2018-08-13 01:15:32', '0');
INSERT INTO `sys_menu` VALUES ('26', '6', '角色修改', '1', 'sys_role_edit', '/upms/role/*', 'PUT', null, null, null, null, '2017-11-08 10:14:41', '2018-08-13 01:15:33', '0');
INSERT INTO `sys_menu` VALUES ('27', '6', '角色删除', '1', 'sys_role_del', '/upms/role/*', 'DELETE', null, null, null, null, '2017-11-08 10:14:59', '2018-08-13 01:15:34', '0');
INSERT INTO `sys_menu` VALUES ('28', '6', '分配权限', '1', 'sys_role_perm', '/upms/role/*', 'PUT', null, null, null, null, '2018-04-20 07:22:55', '2018-08-13 01:15:35', '0');
INSERT INTO `sys_menu` VALUES ('29', '7', '字典查看', '1', null, '/upms/dict/**', 'GET', null, null, null, null, '2017-11-19 22:04:24', '2018-08-13 01:15:37', '0');
INSERT INTO `sys_menu` VALUES ('30', '7', '字典删除', '1', 'sys_dict_del', '/upms/dict/**', 'DELETE', null, null, null, null, '2017-11-29 11:30:11', '2018-08-13 01:15:39', '0');
INSERT INTO `sys_menu` VALUES ('31', '7', '字典新增', '1', 'sys_dict_add', '/upms/dict/**', 'POST', null, null, null, null, '2018-05-11 22:34:55', '2018-08-13 01:15:40', '0');
INSERT INTO `sys_menu` VALUES ('32', '7', '字典修改', '1', 'sys_dict_upd', '/upms/dict/**', 'PUT', null, null, null, null, '2018-05-11 22:36:03', '2018-08-13 01:15:40', '0');
INSERT INTO `sys_menu` VALUES ('33', '8', '日志查看', '1', null, '/upms/log/**', 'GET', null, null, null, null, '2017-11-20 14:07:25', '2018-08-13 01:15:41', '0');
INSERT INTO `sys_menu` VALUES ('34', '8', '日志删除', '1', 'sys_log_del', '/upms/log/*', 'DELETE', null, null, null, null, '2017-11-20 20:37:37', '2018-08-13 01:15:42', '0');
INSERT INTO `sys_menu` VALUES ('35', '2', '服务监控', '0', null, '', null, '', '', '', '1', '2018-01-23 10:53:33', '2018-08-13 01:23:57', '0');
INSERT INTO `sys_menu` VALUES ('36', '2', 'zipkin监控', '0', null, null, null, '', '', null, '2', '2018-01-23 10:55:18', '2018-08-13 01:23:58', '0');
INSERT INTO `sys_menu` VALUES ('37', '2', 'pinpoint监控', '0', null, null, null, '', 'icon-xiazai', null, '3', '2018-01-25 11:08:52', '2018-08-13 01:23:59', '0');
INSERT INTO `sys_menu` VALUES ('38', '2', '缓存状态', '0', null, null, null, '', '', null, '4', '2018-01-23 10:56:11', '2018-08-13 01:24:00', '0');
INSERT INTO `sys_menu` VALUES ('39', '2', 'ELK状态', '0', null, null, null, '', '', null, '5', '2018-01-23 10:55:47', '2018-08-13 01:24:01', '0');
INSERT INTO `sys_menu` VALUES ('40', '2', '接口文档', '0', null, null, null, '', 'icon-wendangguanli', null, '6', '2018-01-23 10:56:43', '2018-08-13 01:24:03', '0');
INSERT INTO `sys_menu` VALUES ('41', '2', '任务监控', '0', null, null, null, '', '', null, '7', '2018-01-23 10:55:18', '2018-08-13 01:24:14', '0');
INSERT INTO `sys_menu` VALUES ('42', '10', '客户端新增', '1', 'sys_client_add', '/upms/client/**', 'POST', null, '1', null, null, '2018-05-15 21:35:18', '2018-08-13 01:16:05', '0');
INSERT INTO `sys_menu` VALUES ('43', '10', '客户端修改', '1', 'sys_client_upd', '/upms/client/**', 'PUT', null, null, null, null, '2018-05-15 21:37:06', '2018-08-13 01:16:09', '0');
INSERT INTO `sys_menu` VALUES ('44', '10', '客户端删除', '1', 'sys_client_del', '/upms/client/**', 'DELETE', null, null, null, null, '2018-05-15 21:39:16', '2018-08-13 01:16:13', '0');
INSERT INTO `sys_menu` VALUES ('45', '10', '客户端查看', '1', null, '/upms/client/**', 'GET', null, null, null, null, '2018-05-15 21:39:57', '2018-08-13 01:16:16', '0');
INSERT INTO `sys_menu` VALUES ('46', '9', '路由查看', '1', null, '/upms/route/**', 'GET', null, null, null, null, '2018-05-15 21:45:59', '2018-08-13 01:16:24', '0');
INSERT INTO `sys_menu` VALUES ('47', '9', '路由新增', '1', 'sys_route_add', '/upms/route/**', 'POST', null, null, null, null, '2018-05-15 21:52:22', '2018-08-13 01:16:25', '0');
INSERT INTO `sys_menu` VALUES ('48', '9', '路由修改', '1', 'sys_route_upd', '/upms/route/**', 'PUT', null, null, null, null, '2018-05-15 21:55:38', '2018-08-13 01:16:28', '0');
INSERT INTO `sys_menu` VALUES ('49', '9', '路由删除', '1', 'sys_route_del', '/upms/route/**', 'DELETE', null, null, null, null, '2018-05-15 21:56:45', '2018-08-13 01:16:30', '0');
INSERT INTO `sys_menu` VALUES ('50', '-1', 'KCloud管理平台1', '0', null, null, null, null, null, null, '1', '2018-08-13 00:59:53', '2018-08-13 01:03:55', '0');
INSERT INTO `sys_menu` VALUES ('51', '50', '21', '0', '21', null, 'GET', null, '21', null, '1', '2018-08-13 16:10:13', null, '0');

-- ----------------------------
-- Table structure for `sys_oauth_client_details`
-- ----------------------------
DROP TABLE IF EXISTS `sys_oauth_client_details`;
CREATE TABLE `sys_oauth_client_details` (
  `client_id` varchar(40) NOT NULL COMMENT '客户端ID',
  `resource_ids` varchar(256) DEFAULT NULL COMMENT '资源服务',
  `client_pwd` varchar(255) DEFAULT NULL COMMENT '客户端密钥-明文[前端展示用]',
  `client_secret` varchar(256) DEFAULT NULL COMMENT '客户端密钥',
  `scope` varchar(256) DEFAULT NULL COMMENT '服务域',
  `authorized_grant_types` varchar(256) DEFAULT NULL COMMENT '授权模式',
  `web_server_redirect_uri` varchar(256) DEFAULT NULL COMMENT '回调地址',
  `authorities` varchar(256) DEFAULT NULL COMMENT '权限',
  `access_token_validity` int(11) DEFAULT NULL COMMENT '请求令牌',
  `refresh_token_validity` int(11) DEFAULT NULL COMMENT '刷新令牌',
  `additional_information` varchar(4096) DEFAULT NULL COMMENT '扩展信息',
  `autoapprove` varchar(256) DEFAULT NULL COMMENT '自动放行',
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_oauth_client_details
-- ----------------------------
INSERT INTO `sys_oauth_client_details` VALUES ('app', null, 'app', '{bcrypt}$2a$10$YoRsFokTvus.HjajeXV7OuSVHviZOhV9Q00VJIZvKJsg4ikLcCma.', 'server', 'password,refresh_token,authorization_code', null, '', null, null, null, 'true');
INSERT INTO `sys_oauth_client_details` VALUES ('kcloud', '', 'root', '{bcrypt}$2a$10$wm4K6AgBry6uGPsQRsdpuevhX1fSKaiUzcPj8aMqX7xXAEE4tYoAe', 'server', 'password,refresh_token', '', '', null, null, '', 'false');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT '角色名称',
  `role_code` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT '角色标识',
  `role_desc` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '角色描述',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `del_flag` char(1) COLLATE utf8mb4_bin DEFAULT '0' COMMENT '删除标识（0-正常,1-删除）',
  PRIMARY KEY (`role_id`) USING BTREE,
  UNIQUE KEY `role_idx1_role_code` (`role_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', 'admin', 'ROLE_ADMIN', '超级管理员', '2017-10-29 15:45:51', '2018-08-02 22:45:17', '0');
INSERT INTO `sys_role` VALUES ('2', '济南办', 'ROLE_JINAN', '济南办角色', '2018-08-03 09:38:04', null, '0');

-- ----------------------------
-- Table structure for `sys_role_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `role_id` int(20) DEFAULT NULL COMMENT '角色ID',
  `dept_id` int(20) DEFAULT NULL COMMENT '部门ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色与部门对应关系';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES ('18', '1', '1');
INSERT INTO `sys_role_dept` VALUES ('24', '2', '40');

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色菜单表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1', '0');
INSERT INTO `sys_role_menu` VALUES ('1', '1');
INSERT INTO `sys_role_menu` VALUES ('1', '2');
INSERT INTO `sys_role_menu` VALUES ('1', '3');
INSERT INTO `sys_role_menu` VALUES ('1', '4');
INSERT INTO `sys_role_menu` VALUES ('1', '5');
INSERT INTO `sys_role_menu` VALUES ('1', '6');
INSERT INTO `sys_role_menu` VALUES ('1', '7');
INSERT INTO `sys_role_menu` VALUES ('1', '8');
INSERT INTO `sys_role_menu` VALUES ('1', '9');
INSERT INTO `sys_role_menu` VALUES ('1', '10');
INSERT INTO `sys_role_menu` VALUES ('1', '11');
INSERT INTO `sys_role_menu` VALUES ('1', '12');
INSERT INTO `sys_role_menu` VALUES ('1', '13');
INSERT INTO `sys_role_menu` VALUES ('1', '14');
INSERT INTO `sys_role_menu` VALUES ('1', '15');
INSERT INTO `sys_role_menu` VALUES ('1', '16');
INSERT INTO `sys_role_menu` VALUES ('1', '17');
INSERT INTO `sys_role_menu` VALUES ('1', '18');
INSERT INTO `sys_role_menu` VALUES ('1', '19');
INSERT INTO `sys_role_menu` VALUES ('1', '20');
INSERT INTO `sys_role_menu` VALUES ('1', '21');
INSERT INTO `sys_role_menu` VALUES ('1', '22');
INSERT INTO `sys_role_menu` VALUES ('1', '23');
INSERT INTO `sys_role_menu` VALUES ('1', '24');
INSERT INTO `sys_role_menu` VALUES ('1', '25');
INSERT INTO `sys_role_menu` VALUES ('1', '26');
INSERT INTO `sys_role_menu` VALUES ('1', '27');
INSERT INTO `sys_role_menu` VALUES ('1', '28');
INSERT INTO `sys_role_menu` VALUES ('1', '29');
INSERT INTO `sys_role_menu` VALUES ('1', '30');
INSERT INTO `sys_role_menu` VALUES ('1', '31');
INSERT INTO `sys_role_menu` VALUES ('1', '32');
INSERT INTO `sys_role_menu` VALUES ('1', '33');
INSERT INTO `sys_role_menu` VALUES ('1', '34');
INSERT INTO `sys_role_menu` VALUES ('1', '35');
INSERT INTO `sys_role_menu` VALUES ('1', '36');
INSERT INTO `sys_role_menu` VALUES ('1', '37');
INSERT INTO `sys_role_menu` VALUES ('1', '38');
INSERT INTO `sys_role_menu` VALUES ('1', '39');
INSERT INTO `sys_role_menu` VALUES ('1', '40');
INSERT INTO `sys_role_menu` VALUES ('1', '41');
INSERT INTO `sys_role_menu` VALUES ('1', '42');
INSERT INTO `sys_role_menu` VALUES ('1', '43');
INSERT INTO `sys_role_menu` VALUES ('1', '44');
INSERT INTO `sys_role_menu` VALUES ('1', '45');
INSERT INTO `sys_role_menu` VALUES ('1', '46');
INSERT INTO `sys_role_menu` VALUES ('1', '47');
INSERT INTO `sys_role_menu` VALUES ('1', '48');
INSERT INTO `sys_role_menu` VALUES ('1', '49');
INSERT INTO `sys_role_menu` VALUES ('1', '50');
INSERT INTO `sys_role_menu` VALUES ('1', '51');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `username` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT '用户名',
  `password` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '随机盐',
  `phone` varchar(20) COLLATE utf8mb4_bin NOT NULL COMMENT '简介',
  `avatar` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '头像',
  `dept_id` int(11) DEFAULT NULL COMMENT '部门ID',
  `status` char(1) COLLATE utf8mb4_bin DEFAULT '0' COMMENT '0-正常，1-删除，9-锁定',
  `account_expired` timestamp NULL DEFAULT NULL COMMENT '账户有效时间止',
  `password_expired` timestamp NULL DEFAULT NULL COMMENT '密码有效时间止',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE KEY `user_idx1_username` (`username`) USING BTREE,
  UNIQUE KEY `user_idx2_phone` (`phone`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '{bcrypt}$2a$10$qhHLMeBs0BGkAp6jFRbORO32mA9ciJTNqrEdB8UlhTgKWanU199di', '', '15165313700', 'http://fastdfs:9999/group1/M00/00/00/wKgLC1tju0eAN1iDAALgUGrM6pk835.png', '1', '0', '2021-10-01 00:00:00', '2021-10-01 00:00:00', '2018-04-20 07:15:18', '2018-08-14 14:17:57');
INSERT INTO `sys_user` VALUES ('2', 'app', '{bcrypt}$2a$10$apdwpbXRQj4RpwKpe461gOmZ26YT9so.GhKitoQE4fe4ghZzfoQjq', null, '18815316660', 'static/img/avator.gif', '1', '1', '2018-07-24 10:40:09', '2018-07-22 10:40:09', '2018-07-22 23:45:12', '2018-08-18 16:05:56');
INSERT INTO `sys_user` VALUES ('8', 'kcloud', '{bcrypt}$2a$10$ZulPFQG6j5dfDsj9K70VZeNWmAnRhUDexyBJybXMHNssqM8P1Pkhq', null, '18888888888', null, '40', '0', '2018-08-31 00:00:00', '2018-08-31 00:00:00', '2018-08-02 21:48:55', '2018-08-18 16:05:37');
INSERT INTO `sys_user` VALUES ('9', 'lix', '{bcrypt}$2a$10$5IxzpjkrBy0ubkJXSIFUxOh/ayGfnYV0M9CTRgKx8L4UBeGQtrFhq', null, '15753756144', 'static/img/avator.gif', '40', '0', '2020-08-29 00:00:00', '2020-08-29 00:00:00', '2018-08-18 16:05:25', '2018-08-18 22:52:08');

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户角色表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('2', '1');
INSERT INTO `sys_user_role` VALUES ('8', '2');
INSERT INTO `sys_user_role` VALUES ('9', '1');

-- ----------------------------
-- Table structure for `zipkin_annotations`
-- ----------------------------
DROP TABLE IF EXISTS `zipkin_annotations`;
CREATE TABLE `zipkin_annotations` (
  `trace_id_high` bigint(20) NOT NULL DEFAULT '0' COMMENT 'If non zero, this means the trace uses 128 bit traceIds instead of 64 bit',
  `trace_id` bigint(20) NOT NULL COMMENT 'coincides with zipkin_spans.trace_id',
  `span_id` bigint(20) NOT NULL COMMENT 'coincides with zipkin_spans.id',
  `a_key` varchar(255) NOT NULL COMMENT 'BinaryAnnotation.key or Annotation.value if type == -1',
  `a_value` blob COMMENT 'BinaryAnnotation.value(), which must be smaller than 64KB',
  `a_type` int(11) NOT NULL COMMENT 'BinaryAnnotation.type() or -1 if Annotation',
  `a_timestamp` bigint(20) DEFAULT NULL COMMENT 'Used to implement TTL; Annotation.timestamp or zipkin_spans.timestamp',
  `endpoint_ipv4` int(11) DEFAULT NULL COMMENT 'Null when Binary/Annotation.endpoint is null',
  `endpoint_ipv6` binary(16) DEFAULT NULL COMMENT 'Null when Binary/Annotation.endpoint is null, or no IPv6 address',
  `endpoint_port` smallint(6) DEFAULT NULL COMMENT 'Null when Binary/Annotation.endpoint is null',
  `endpoint_service_name` varchar(255) DEFAULT NULL COMMENT 'Null when Binary/Annotation.endpoint is null',
  UNIQUE KEY `trace_id_high` (`trace_id_high`,`trace_id`,`span_id`,`a_key`,`a_timestamp`) USING BTREE COMMENT 'Ignore insert on duplicate',
  UNIQUE KEY `trace_id_high_4` (`trace_id_high`,`trace_id`,`span_id`,`a_key`,`a_timestamp`) USING BTREE COMMENT 'Ignore insert on duplicate',
  KEY `trace_id_high_2` (`trace_id_high`,`trace_id`,`span_id`) USING BTREE COMMENT 'for joining with zipkin_spans',
  KEY `trace_id_high_3` (`trace_id_high`,`trace_id`) USING BTREE COMMENT 'for getTraces/ByIds',
  KEY `endpoint_service_name` (`endpoint_service_name`) USING BTREE COMMENT 'for getTraces and getServiceNames',
  KEY `a_type` (`a_type`) USING BTREE COMMENT 'for getTraces',
  KEY `a_key` (`a_key`) USING BTREE COMMENT 'for getTraces',
  KEY `trace_id` (`trace_id`,`span_id`,`a_key`) USING BTREE COMMENT 'for dependencies job',
  KEY `trace_id_high_5` (`trace_id_high`,`trace_id`,`span_id`) USING BTREE COMMENT 'for joining with zipkin_spans',
  KEY `trace_id_high_6` (`trace_id_high`,`trace_id`) USING BTREE COMMENT 'for getTraces/ByIds',
  KEY `endpoint_service_name_2` (`endpoint_service_name`) USING BTREE COMMENT 'for getTraces and getServiceNames',
  KEY `a_type_2` (`a_type`) USING BTREE COMMENT 'for getTraces',
  KEY `a_key_2` (`a_key`) USING BTREE COMMENT 'for getTraces',
  KEY `trace_id_2` (`trace_id`,`span_id`,`a_key`) USING BTREE COMMENT 'for dependencies job'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;

-- ----------------------------
-- Records of zipkin_annotations
-- ----------------------------

-- ----------------------------
-- Table structure for `zipkin_dependencies`
-- ----------------------------
DROP TABLE IF EXISTS `zipkin_dependencies`;
CREATE TABLE `zipkin_dependencies` (
  `day` date NOT NULL,
  `parent` varchar(255) NOT NULL,
  `child` varchar(255) NOT NULL,
  `call_count` bigint(20) DEFAULT NULL,
  UNIQUE KEY `day` (`day`,`parent`,`child`) USING BTREE,
  UNIQUE KEY `day_2` (`day`,`parent`,`child`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;

-- ----------------------------
-- Records of zipkin_dependencies
-- ----------------------------

-- ----------------------------
-- Table structure for `zipkin_spans`
-- ----------------------------
DROP TABLE IF EXISTS `zipkin_spans`;
CREATE TABLE `zipkin_spans` (
  `trace_id_high` bigint(20) NOT NULL DEFAULT '0' COMMENT 'If non zero, this means the trace uses 128 bit traceIds instead of 64 bit',
  `trace_id` bigint(20) NOT NULL,
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `debug` bit(1) DEFAULT NULL,
  `start_ts` bigint(20) DEFAULT NULL COMMENT 'Span.timestamp(): epoch micros used for endTs query and to implement TTL',
  `duration` bigint(20) DEFAULT NULL COMMENT 'Span.duration(): micros used for minDuration and maxDuration query',
  UNIQUE KEY `trace_id_high` (`trace_id_high`,`trace_id`,`id`) USING BTREE COMMENT 'ignore insert on duplicate',
  UNIQUE KEY `trace_id_high_4` (`trace_id_high`,`trace_id`,`id`) USING BTREE COMMENT 'ignore insert on duplicate',
  KEY `trace_id_high_2` (`trace_id_high`,`trace_id`,`id`) USING BTREE COMMENT 'for joining with zipkin_annotations',
  KEY `trace_id_high_3` (`trace_id_high`,`trace_id`) USING BTREE COMMENT 'for getTracesByIds',
  KEY `name` (`name`) USING BTREE COMMENT 'for getTraces and getSpanNames',
  KEY `start_ts` (`start_ts`) USING BTREE COMMENT 'for getTraces ordering and range',
  KEY `trace_id_high_5` (`trace_id_high`,`trace_id`,`id`) USING BTREE COMMENT 'for joining with zipkin_annotations',
  KEY `trace_id_high_6` (`trace_id_high`,`trace_id`) USING BTREE COMMENT 'for getTracesByIds',
  KEY `name_2` (`name`) USING BTREE COMMENT 'for getTraces and getSpanNames',
  KEY `start_ts_2` (`start_ts`) USING BTREE COMMENT 'for getTraces ordering and range'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;

-- ----------------------------
-- Records of zipkin_spans
-- ----------------------------
