package com.emsite.auth.feign.fallback;
import com.emsite.auth.feign.UserService;
import com.emsite.common.vo.UserVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
/**
* @Description: 用户服务的fallback熔断
* @author lix
* @date 2018/8/20 14:59
*/
@Slf4j
@Component
public class UserServiceFallbackImpl implements UserService {
    /**
     ============================================
     * 描   述：通过用户名查询用户、角色信息
     * 参   数：[username]
     * 返回类型：UserVO
     * 创 建 人：Jerry
     * 创建时间：2018/6/26 14:48
     ============================================
     */
    @Override
    public UserVO findUserByUsername(String username) {
        log.error("调用{}异常:{}", "findUserByUsername", username);
        return null;
    }

    /**
     ============================================
     * 描   述：通过手机号查询用户、角色信息
     * 参   数：[mobile手机号]
     * 返回类型：com.emsite.common.vo.UserVO
     * 创 建 人：Jerry
     * 创建时间：2018/6/26 14:54
     ============================================
     */
    @Override
    public UserVO findUserByMobile(String mobile) {
        log.error("调用{}异常:{}", "通过手机号查询用户", mobile);
        return null;
    }

    /**
     ============================================
     * 描   述： 根据OpenId查询用户信息
     * 参   数：[openId]
     * 返回类型：com.emsite.common.vo.UserVO
     * 创 建 人：Jerry
     * 创建时间：2018/6/26 14:54
     ============================================
     */
    @Override
    public UserVO findUserByOpenId(String openId) {
        log.error("调用{}异常:{}", "通过OpenId查询用户", openId);
        return null;
    }
}
