package com.emsite.auth.config;

import lombok.Data;
/**
* @Description: social 登录基础配置
* @author lix
* @date 2018/8/20 14:58
*/
@Data
public class SocialPropertiesConfig {
    /**
     * 提供商
     */
    private String providerId;
    /**
     * 应用ID
     */
    private String clientId;
    /**
     * 应用密钥
     */
    private String clientSecret;

}