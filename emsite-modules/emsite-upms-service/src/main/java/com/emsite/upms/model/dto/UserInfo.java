package com.emsite.upms.model.dto;
import com.emsite.upms.model.entity.SysUser;
import lombok.Data;

import java.io.Serializable;
/**
* @Description: 用户、权限、角色
* @author lix
* @date 2018/8/20 15:12
*/
@Data
public class UserInfo implements Serializable {
    /**
     * 用户基本信息
     */
    private SysUser sysUser;
    /**
     * 权限标识集合
     */
    private String[] permissions;

    /**
     * 角色集合
     */
    private String[] roles;
}