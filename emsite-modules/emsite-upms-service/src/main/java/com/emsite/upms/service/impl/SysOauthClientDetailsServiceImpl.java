package com.emsite.upms.service.impl;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.emsite.upms.mapper.SysOauthClientDetailsMapper;
import com.emsite.upms.model.entity.SysOauthClientDetails;
import com.emsite.upms.service.SysOauthClientDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
/**
* @Description: 授权服务实现类
* @author lix
* @date 2018/8/20 15:17
*/
@Service
public class SysOauthClientDetailsServiceImpl extends ServiceImpl<SysOauthClientDetailsMapper, SysOauthClientDetails> implements SysOauthClientDetailsService {
    private static final PasswordEncoder ENCODER = PasswordEncoderFactories.createDelegatingPasswordEncoder();

    @Override
    public boolean insert(SysOauthClientDetails entity) {
        entity.setClientSecret(ENCODER.encode(entity.getClientPwd()));
        return super.insert(entity);
    }

    @Override
    public boolean updateById(SysOauthClientDetails entity) {
        entity.setClientSecret(ENCODER.encode(entity.getClientPwd()));
        return super.updateById(entity);
    }
}
