package com.emsite.upms.model.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;

import java.io.Serializable;
/**
* @Description: 客户端授权
* @author lix
* @date 2018/8/20 15:15
*/
@Data
@TableName("sys_oauth_client_details")
public class SysOauthClientDetails extends Model<SysOauthClientDetails> {
    private static final long serialVersionUID = 1L;

    @TableId(value = "client_id", type = IdType.INPUT)
    private String clientId;
    @TableField("resource_ids")
    private String resourceIds;
    @TableField("client_secret")
    private String clientSecret;
    @TableField("client_pwd")
    private String clientPwd;
    @TableField("scope")
    private String scope;
    @TableField("authorized_grant_types")
    private String authorizedGrantTypes;
    @TableField("web_server_redirect_uri")
    private String webServerRedirectUri;
    private String authorities;
    @TableField("access_token_validity")
    private Integer accessTokenValidity;
    @TableField("refresh_token_validity")
    private Integer refreshTokenValidity;
    @TableField("additional_information")
    private String additionalInformation;
    @TableField("autoapprove")
    private String autoapprove;

    @Override
    protected Serializable pkVal() {
        return this.clientId;
    }

    @Override
    public String toString() {
        return "SysOauthClientDetails{" +
                ", clientId=" + clientId +
                ", resourceIds=" + resourceIds +
                ", clientPwd=" + clientPwd +
                ", clientSecret=" + clientSecret +
                ", scope=" + scope +
                ", authorizedGrantTypes=" + authorizedGrantTypes +
                ", webServerRedirectUri=" + webServerRedirectUri +
                ", authorities=" + authorities +
                ", accessTokenValidity=" + accessTokenValidity +
                ", refreshTokenValidity=" + refreshTokenValidity +
                ", additionalInformation=" + additionalInformation +
                ", autoapprove=" + autoapprove +
                "}";
    }
}
