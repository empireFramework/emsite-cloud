package com.emsite.upms.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.emsite.upms.mapper.SysUserRoleMapper;
import com.emsite.upms.model.entity.SysUserRole;
import com.emsite.upms.service.SysUserRoleService;
import org.springframework.stereotype.Service;
/**
* @Description: 用户角色表 服务实现类
* @author lix
* @date 2018/8/20 15:19
*/
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

    /**
     * 根据用户Id删除该用户的角色关系
     */
    @Override
    public Boolean deleteByUserId(Integer userId) {
        return baseMapper.deleteByUserId(userId);
    }
}
