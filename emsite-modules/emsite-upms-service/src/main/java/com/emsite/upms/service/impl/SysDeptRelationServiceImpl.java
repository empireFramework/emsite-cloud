package com.emsite.upms.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.emsite.upms.mapper.SysDeptRelationMapper;
import com.emsite.upms.model.entity.SysDeptRelation;
import com.emsite.upms.service.SysDeptRelationService;
import org.springframework.stereotype.Service;

/**
* @Description: 部门间关系服务实现类
* @author lix
* @date 2018/8/20 15:16
*/
@Service
public class SysDeptRelationServiceImpl extends ServiceImpl<SysDeptRelationMapper, SysDeptRelation> implements SysDeptRelationService {

}
