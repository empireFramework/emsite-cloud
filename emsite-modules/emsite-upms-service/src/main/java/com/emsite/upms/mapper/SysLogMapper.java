package com.emsite.upms.mapper;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.emsite.common.entity.SysLog;

public interface SysLogMapper extends BaseMapper<SysLog> {

}