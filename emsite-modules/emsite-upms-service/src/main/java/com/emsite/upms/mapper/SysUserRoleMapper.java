package com.emsite.upms.mapper;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.emsite.upms.model.entity.SysUserRole;
import org.apache.ibatis.annotations.Param;

public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

    Boolean deleteByUserId(@Param("userId") Integer userId);
}