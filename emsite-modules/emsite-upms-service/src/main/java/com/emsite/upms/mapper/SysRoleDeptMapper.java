package com.emsite.upms.mapper;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.emsite.upms.model.entity.SysRoleDept;

public interface SysRoleDeptMapper extends BaseMapper<SysRoleDept> {

}