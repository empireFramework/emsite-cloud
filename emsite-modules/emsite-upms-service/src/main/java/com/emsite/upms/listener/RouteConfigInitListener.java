package com.emsite.upms.listener;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.emsite.common.constant.CommonConstant;
import com.emsite.common.entity.SysGatewayRoute;
import com.emsite.upms.service.SysGatewayRouteService;
import com.xiaoleilu.hutool.collection.CollUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
/**
* @Description: 预加载路由到Redis
* @author lix
* @date 2018/8/20 15:04
*/
@Slf4j
@Component
public class RouteConfigInitListener implements CommandLineRunner {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private SysGatewayRouteService sysGatewayRouteService;
    /**
     * Callback used to run the bean.
     * 初始化路由配置的数据，避免gateway 依赖业务模块
     *
     */
    @Override
    public void run(String... args) {
        log.info("开始初始化路由配置数据");
        EntityWrapper wrapper = new EntityWrapper();
        wrapper.eq(CommonConstant.DEL_FLAG, CommonConstant.STATUS_NORMAL);
        List<SysGatewayRoute> routeList = sysGatewayRouteService.selectList(wrapper);
        if (CollUtil.isNotEmpty(routeList)) {
            redisTemplate.opsForValue().set(CommonConstant.ROUTE_KEY, routeList);
            log.info("更新Redis中路由配置数据：{}条", routeList.size());
        }
        log.info("初始化路由配置数据完毕");
    }
}