package com.emsite.upms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * ============================================
 * 描  述：通用用户权限管理
 * 包  名：com.kcloud.system.upms
 * 类  名：SystemApplication
 * 创建人：Jerry
 * 创建时间：2018/7/3 11:28
 * ============================================
 **/
@EnableAsync
@EnableHystrix
@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(basePackages = {"com.emsite.upms", "com.emsite.common.bean"})
public class EmsiteUpmsApplication {
    public static void main (String [] args) {
        SpringApplication.run(EmsiteUpmsApplication.class,args);
    }
//    @Configuration
//    @EnableResourceServer
//    public class ResourceSecurityConfigAdapter extends ResourceServerConfigurerAdapter {
//        @Override
//        public void configure(HttpSecurity http) throws Exception {
//            http.authorizeRequests()
//                // 访问下面路径无需Token认证
//                .antMatchers(
//                        "/actuator/**",
//                        "/user/findUserByUsername/**",
//                        "/user/findUserByMobile/**",
//                        "/user/findUserByOpenId/**",
//                        "/webjars/**",
//                        "/swagger-resources/**",
//                        "/**/v2/api-docs",
//                        "/swagger-ui.html")
//                .permitAll()
//                // 除了上面配置的路径都需要Token认证
//                .anyRequest()
//                .authenticated()
//                .and()
//                // 暂时禁用CSRF，否则无法提交表单
//                .csrf().disable();
//        }
//    }
}
