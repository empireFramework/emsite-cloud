package com.emsite.upms.model.dto;
import com.emsite.upms.model.entity.SysUser;
import lombok.Data;

import java.util.List;
/**
* @Description: 角色、部门、新密码
* @author lix
* @date 2018/8/20 15:12
*/
@Data
public class UserDTO extends SysUser {

    private List<Integer> role;

    private Integer deptId;

    private String newpassword1;
}
