package com.emsite.upms.mapper;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.emsite.upms.model.entity.SysDict;

public interface SysDictMapper extends BaseMapper<SysDict> {
}