package com.emsite.upms.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.emsite.common.entity.SysGatewayRoute;

public interface SysGatewayRouteMapper extends BaseMapper<SysGatewayRoute> {

}
